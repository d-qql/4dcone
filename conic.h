#pragma once
#include <numbers>

#include "utils/utils.h"



inline mat4 transformatedSurface4D(mat4 transformation) {
    return transpose(transformation) *
           mat4(
               vec4(1.f, 0.f, 0.f, 0.f),
               vec4(0.f, 1.f, 0.f, 0.f),
               vec4(0.f, 0.f, -1.f, 0.f),
               vec4(0.f, 0.f, 0.f, 0.f)
           ) *
           transformation;
}

inline mat4 zwRotation(float angle) {
    angle *= std::numbers::pi_v<float> / 180.f;
    return transpose(mat4(
        vec4(1.f, 0.f, 0.f, 0.f),
        vec4(0.f, 1.f, 0.f, 0.f),
        vec4(0.f, 0.f, std::cos(angle), -std::sin(angle)),
        vec4(0.f, 0.f, std::sin(angle), std::cos(angle))
    ));
}
