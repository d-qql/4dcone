#pragma once
#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include <algorithm>
#include "utils/utils.h"

#include <imgui.h>
#include <imgui/backends/imgui_impl_glfw.h>
#include <imgui/backends/imgui_impl_opengl3.h>

struct GUI {
    GLFWwindow *window;

    float mouseX = 0.f, mouseY = 0.f, mouseZ = 5.f;
    bool show = true;

    float rotation = 0.f;
    mat4 rotationMatrix;
    mat4 surfaceMatrix;

    GUI(GLFWwindow *const pWindow) noexcept;

    ~GUI() {
        ImGui_ImplOpenGL3_Shutdown();
        ImGui_ImplGlfw_Shutdown();
        ImGui::DestroyContext();
    }

    void render() noexcept;

    void setRotationMatrix(mat4 matrix) noexcept;

    void setSurfaceMatrix(mat4 matrix) noexcept;
};

inline GUI::GUI(GLFWwindow *const pWindow) noexcept
    : window(pWindow) {
    glfwSetWindowUserPointer(window, this);
    glfwSetKeyCallback(window, +[](GLFWwindow *const w, int key, int, int action, int) noexcept {
        if (action == GLFW_RELEASE)
            return;

        GUI &gui = *reinterpret_cast<GUI *>(glfwGetWindowUserPointer(w));
        if (key == GLFW_KEY_ESCAPE)
            gui.show = !gui.show;
    });

    IMGUI_CHECKVERSION();
    ImGui::CreateContext();

    ImGui::StyleColorsDark();
    //ImGui::StyleColorsClassic();

    ImGui_ImplGlfw_InitForOpenGL(window, true);
    ImGui_ImplOpenGL3_Init("#version 330 core");
}

inline void GUI::setRotationMatrix(mat4 matrix) noexcept {
    rotationMatrix = matrix;
}

inline void GUI::setSurfaceMatrix(mat4 matrix) noexcept {
    surfaceMatrix = matrix;
}

inline void GUI::render() noexcept {
    //glfwSetInputMode(window, GLFW_CURSOR, show ? GLFW_CURSOR_NORMAL : GLFW_CURSOR_DISABLED);

    ImGuiIO &io = ImGui::GetIO();
    ImGui_ImplOpenGL3_NewFrame();
    ImGui_ImplGlfw_NewFrame();
    ImGui::NewFrame();

    if (!io.WantCaptureMouse) {
        float const dz = 1e-2f * io.MouseWheel;
        mouseZ = std::clamp(mouseZ - dz, 0.5f, 10.f);

        if (io.MouseDown[0]) {
            auto const [dx, dy] = io.MouseDelta;

            float const m = 1e-3f;
            mouseX -= m * dx;
            mouseY = std::clamp(mouseY + m * dy, -0.999f, 0.999f);
        }
    }

    if (show) {
        ImGui::SetNextWindowPos(ImVec2(0.f, 0.f));
        ImGui::Begin("Информация", nullptr, ImGuiWindowFlags_NoMove);
        if (ImGui::Button("Quit"))
            glfwSetWindowShouldClose(window, GLFW_TRUE);
        ImGui::Text("%.2f ms, %.1f FPS", 1000.0f / io.Framerate, io.Framerate);
        ImGui::Text("ESC to toggle GUI focus");
        ImGui::Text("%g", io.MouseWheel);
        ImGui::SliderFloat("Угол поворота", &rotation, 0.f, 90.f, "%.1f", ImGuiSliderFlags_None);

        ImGui::Text("Матрица поворота:\n"
                    "%.3f,  %.3f,  %.3f,  %.3f\n"
                    "%.3f,  %.3f,  %.3f,  %.3f\n"
                    "%.3f,  %.3f,  %.3f,  %.3f\n"
                    "%.3f,  %.3f,  %.3f,  %.3f\n",
                    rotationMatrix[0][0], rotationMatrix[1][0], rotationMatrix[2][0], rotationMatrix[3][0],
                    rotationMatrix[0][1], rotationMatrix[1][1], rotationMatrix[2][1], rotationMatrix[3][1],
                    rotationMatrix[0][2], rotationMatrix[1][2], rotationMatrix[2][2], rotationMatrix[3][2],
                    rotationMatrix[0][3], rotationMatrix[1][3], rotationMatrix[2][3], rotationMatrix[3][3]);

        ImGui::Text("Матрица поверхности:\n"
                    "%.3f,  %.3f,  %.3f,  %.3f\n"
                    "%.3f,  %.3f,  %.3f,  %.3f\n"
                    "%.3f,  %.3f,  %.3f,  %.3f\n"
                    "%.3f,  %.3f,  %.3f,  %.3f\n",
                    surfaceMatrix[0][0], surfaceMatrix[1][0], surfaceMatrix[2][0], surfaceMatrix[3][0],
                    surfaceMatrix[0][1], surfaceMatrix[1][1], surfaceMatrix[2][1], surfaceMatrix[3][1],
                    surfaceMatrix[0][2], surfaceMatrix[1][2], surfaceMatrix[2][2], surfaceMatrix[3][2],
                    surfaceMatrix[0][3], surfaceMatrix[1][3], surfaceMatrix[2][3], surfaceMatrix[3][3]);
        ImGui::End();
    }

    ImGui::Render();
    ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());
}
