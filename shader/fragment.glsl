#version 330 core
in vec2 uv;
layout(location = 0) out vec4 outColor;

uniform float uTime;
uniform float uAspectRatio;
uniform vec3 uMouse;
uniform float uRotationAngle;
uniform mat4 uSurface;

mat4 lookAt(vec3 eye, vec3 at, vec3 up)
{
    vec3 z = normalize(eye - at);
    vec3 x = normalize(cross(up, z));
    vec3 y = cross(z, x);
    return mat4
    (
    vec4(x.x, y.x, z.x, 0.f),
    vec4(x.y, y.y, z.y, 0.f),
    vec4(x.z, y.z, z.z, 0.f),
    vec4(-eye * mat3(x, y, z), 1.f)
    );
}

vec3 scaleFrom(float tgFOV, float aspectRatio)
{
    return vec3(tgFOV * aspectRatio, tgFOV, 1.f);
}

struct Ray
{
    vec3 origin;
    vec3 direction;
};

vec3 rayPoint(Ray ray, float t)
{
    return ray.origin + ray.direction * t;
}

Ray castRay(vec3 origin, mat3 cameraBasis, vec3 scale, vec2 uv)
{
    return Ray(
    origin,
    normalize(cameraBasis * (scale * vec3(uv, -1.f)))
    );
}

float conic(Ray ray, float t){
    vec3 r = rayPoint(ray, t);
    return r.x * r.x + r.y * r.y + r.z * r.z - 1;
}

float dConicDt(Ray ray, float t)
{
    return 2.f * dot(rayPoint(ray, t), ray.direction);
}

vec3 conicNormal(Ray ray, float t)
{
    vec3 r = rayPoint(ray, t);
    return normalize(vec3(
    r.x,
    r.y,
    r.z
    ));
}

float axisX(Ray ray, float t)
{
    vec3 r = rayPoint(ray, t);
    return r.y * r.y + r.z * r.z - 1e-16f;
}

float dAxisXdT(Ray ray, float t)
{
    vec3 r = rayPoint(ray, t);
    return 2.f * dot(r, ray.direction) - 2.f * r.x * ray.direction.x;
}

float axisY(Ray ray, float t)
{
    vec3 r = rayPoint(ray, t);
    return r.x * r.x + r.z * r.z - 1e-16f;
}

float dAxisYdT(Ray ray, float t)
{
    vec3 r = rayPoint(ray, t);
    return 2.f * dot(r, ray.direction) - 2.f * r.y * ray.direction.y;
}

float axisZ(Ray ray, float t)
{
    vec3 r = rayPoint(ray, t);
    return r.x * r.x + r.y * r.y - 1e-16f;
}

float dAxisZdT(Ray ray, float t)
{
    vec3 r = rayPoint(ray, t);
    return 2.f * dot(r, ray.direction) - 2.f * r.z * ray.direction.z;
}

float intersect(Ray ray)
{
    float t = 0;
    int iters = 0;

    while (abs(conic(ray, t)) > 1e-5){
        if (iters > 10) return 0.f;
        t = t - conic(ray, t) / dConicDt(ray, t);
        iters += 1;
    }
    return t;
}

float intersectX(Ray ray)
{
    float t = 0;
    int iters = 0;

    while (abs(axisX(ray, t)) > 1e-5){
        if (iters > 10) return 0.f;
        t = t - axisX(ray, t) / dAxisXdT(ray, t);
        iters += 1;
    }
    return t;
}

float intersectY(Ray ray)
{
    float t = 0;
    int iters = 0;

    while (abs(axisY(ray, t)) > 1e-5){
        if (iters > 10) return 0.f;
        t = t - axisY(ray, t) / dAxisYdT(ray, t);
        iters += 1;
    }
    return t;
}

float intersectZ(Ray ray)
{
    float t = 0;
    int iters = 0;

    while (abs(axisZ(ray, t)) > 1e-5){
        if (iters > 10) return 0.f;
        t = t - axisZ(ray, t) / dAxisZdT(ray, t);
        iters += 1;
    }
    return t;
}

//mat4 transformatedSurface4D(mat4 transformation)
//{
//    return transpose(transformation) *
//    mat4(
//    vec4(1.f, 0.f, 0.f, 0.f),
//    vec4(0.f, 1.f, 0.f, 0.f),
//    vec4(0.f, 0.f, -1.f, 0.f),
//    vec4(0.f, 0.f, 0.f, 0.f)
//    ) *
//    transformation;
//}

float surfaceValue(mat4 surface4d, Ray ray, float t)
{
    vec4 r = vec4(rayPoint(ray, t), 1.f);
    return dot(r, surface4d * r);
}

vec4 surface4dGrad(mat4 surface4d, vec4 r)
{
    return (transpose(surface4d) + surface4d) * r;
}

float surface4dGradDt(mat4 surface4d, Ray ray, float t){
    vec4 r = vec4(rayPoint(ray, t), 1.f);
    return dot(surface4dGrad(surface4d, r), vec4(ray.direction, 0.f));
}

float intersectSurface4D(mat4 surface, Ray ray)
{
    float t = 0;
    int iters = 0;

    while (abs(surfaceValue(surface, ray, t)) > 1e-5){
        if (iters > 10) return 0.f;
        t = t - surfaceValue(surface, ray, t) / surface4dGradDt(surface, ray, t);
        iters += 1;
    }
    return t;
}

void main()
{

    // projection parameters:
    float ctgFOV = 1.5f;
    float f = 10.f;
    float n = 0.1f;

    float C1 =      (f + n) / (f - n);
    float C2 = 2.f * f * n  / (f - n);

    mat4 proj = mat4
    (
    ctgFOV / uAspectRatio, 0.f, 0.f, 0.f,
    0.f, ctgFOV, 0.f, 0.f,
    0.f, 0.f, -C1, -1.f,
    0.f, 0.f, -C2, 0.f
    );

    float pi = 3.1415926535f;
    float phi   =        pi * uMouse.x;
    float theta = 0.5f * pi * uMouse.y;
    vec3 camPos = uMouse.z * vec3
    (
    cos(theta) * cos(phi),
    cos(theta) * sin(phi),
    sin(theta)
    );
    mat4 R = lookAt(camPos, vec3(0.f), vec3(0.f, 0.f, 1.f));

    Ray ray = castRay(camPos, transpose(mat3(R)), scaleFrom(1./ctgFOV, uAspectRatio), uv);

    outColor = intersectX(ray) != 0 ? vec4(1.f, 0.f, 0.f, 1.f) : vec4(vec3(0.f), 1.f);
    outColor = intersectY(ray) != 0 ? vec4(0.f, 1.f, 0.f, 1.f) : outColor;
    outColor = intersectZ(ray) != 0 ? vec4(0.f, 0.f, 1.f, 1.f) : outColor;

//    float rotationDeg = radians(uRotationAngle);
//    mat4 transformation = transpose(mat4(
//    vec4(1.f, 0.f, 0.f, 0.f),
//    vec4(0.f, 1.f, 0.f, 0.f),
//    vec4(0.f, 0.f, cos(rotationDeg), -sin(rotationDeg)),
//    vec4(0.f, 0.f, sin(rotationDeg), cos(rotationDeg))
//    ));
//
//    mat4 surface = transformatedSurface4D(transformation);
    mat4 surface = uSurface;

    float intersectT = intersectSurface4D(surface, ray);
    //    float normalLight = dot(conicNormal(ray, intersectT), normalize(vec3(0.f, 0.f, 1.f)));
    //    outColor = intersectT != 0 ? (normalLight > 0 ? vec4(vec3(normalLight), 1.f) : vec4(vec3(0.f), 1.f)) : outColor;
    vec4 intersectionPoint = vec4(rayPoint(ray, intersectT), 1.f);
    vec4 normal = normalize(surface4dGrad(surface, intersectionPoint));
    outColor = intersectT != 0 ? vec4(fract(intersectionPoint.x), fract(intersectionPoint.y), fract(intersectionPoint.z), 1.f) : outColor;
}
